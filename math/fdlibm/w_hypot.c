
/* @(#)w_hypot.c 1.3 95/01/18 */
/*
 * ====================================================
 * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
 *
 * Developed at SunSoft, a Sun Microsystems, Inc. business.
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice 
 * is preserved.
 * ====================================================
 */

/*
 * wrapper fd_hypot(x,y)
 */

#include "fdlibm.h"
#include "fdlibm_intern.h"

#ifdef __STDC__
	double fd_hypot(double x, double y)/* wrapper fd_hypot */
#else
	double fd_hypot(x,y)		/* wrapper fd_hypot */
	double x,y;
#endif
{
#ifdef FD_IEEE_LIBM
	return __ieee754_hypot(x,y);
#else
	double z;
	z = __ieee754_hypot(x,y);
	if(_LIB_VERSION == _IEEE_) return z;
	if((!fd_finite(z))&&fd_finite(x)&&fd_finite(y))
	    return __kernel_standard(x,y,4); /* fd_hypot overflow */
	else
	    return z;
#endif
}
