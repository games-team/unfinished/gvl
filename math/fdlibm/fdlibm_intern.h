#ifndef UUID_42BBB6C432494D09B3881EA0734FB3E1
#define UUID_42BBB6C432494D09B3881EA0734FB3E1

#if defined(__GNUC__)
#define FD_INLINE static inline
#elif defined(_MSC_VER)
#define FD_INLINE __inline
#endif

#include "../ieee.hpp"

#endif // UUID_42BBB6C432494D09B3881EA0734FB3E1
